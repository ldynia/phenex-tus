# dtu-flask-tus

[tus](https://www.tus.io) server implementation for [Flask](https://flask.pocoo.org). Originaly developed by [volsen](https://github.com/volesen/flask-tus) extended for needs of DTU Food.

## Source
* `flask-tus` directory contains forked clone [flask-tus](https://github.com/mkoliba/flask-tus) which is local dependency of this module
* `dtu_fask_tus` contain source of this modul

## Requirements

Following software stack is required to run this project:

- [python3](https://www.python.org/)
- [virtualenv](https://virtualenv.pypa.io/en/latest/installation/)
- [pip](https://pip.pypa.io/en/stable/)

## Installation

### virtualenviroment

if you do not have yet, install virtual enviroment for your flask project:

```bash
$ virtualenv -p python3 venv
$ source venv/bin/activate
```

Clone and install Flask module:

```bash
$ git clone git@bitbucket.org:genomicepidemiology/phenex-tus.git
$ pip install -e ./dtu-flask-tus/flask-tus
$ pip install -e ./dtu-flask-tus
```

## Use

The dtu_flask_tus extension, need to be instatiated with an app and a model and a database/session object.
In your Flask application `__init__.py` file import and initialise:

```python
from flask import Flask
from flask_mongoengine import MongoEngine
from dtu_flask_tus import FlaskTusExtended
from dtu_flask_tus.models import UploadModel

app = Flask(__name__)
app.config['MONGODB_SETTINGS'] = = {
        'host': 'mongodb://db_host/database_name,
        'username': 'mongodb_username',
        'password': 'password'
        }

mongo = MongoEngine(app)
flask_tus = FlaskTusExtended(app, model=UploadModel, db=mongo)
```

If you use factory patter inicialise database and dtu_flask_tus module using `init_app()` method

```python
from config import Config # import configuration class

mongo = MongoEngine()
flask_tus = FlaskTusExtended()

def create_app(config_class=Config):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(config_class)

    mongo.init_app(app)
    flask_tus.init_app(app, model=UploadModel, db=mongo)
    return app
```

## Configuration

Settings are added in app.config:

- `TUS_SAMPLE_URL` - URL of sample metadata endpoint, relative to flask app URL
- `TUS_UPLOAD_URL` - URL of file upload endpoint
- `TUS_UPLOAD_DIR` - Path to upload directory. Can be changed with custom models (eg. for saving in AWS S3)
- `TUS_MAX_SIZE` - Max size of a file-upload
- `TUS_EXPIRATION` - Time allowed to complete upload (Must be insatce of datetime.timedelta)
- `TUS_CHUNK_SIZE` - Chunk size used in calculation of MD5

example:

```python
MONGODB_SETTINGS = {
        'host': os.environ.get('MONGO_TUSFULL_HOST'),
        'username': os.environ.get('MONGO_TUSFULL_USERNAME'),
        'password': os.environ.get('MONGO_TUSFULL_PASSWORD')
        }

TUS_EXPIRATION = datetime.timedelta(days=365)
TUS_UPLOAD_DIR = '/uplodas/' # default is temporary directory
# optional
TUS_SAMPLE_URL = '/samples/'
TUS_UPLOAD_URL = '/files/
```

Following enviromental variabless needs to be set if you use above example

- `MONGO_TUSFULL_HOST` - format: mongodb://db_host/database_name
- `MONGO_TUSFULL_USERNAME`
- `MONGO_TUSFULL_PASSWORD`
