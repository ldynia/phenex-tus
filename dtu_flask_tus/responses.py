from flask import jsonify


def sample_response(success=False, message="", data=None):
    return jsonify(
        {
            "success": success,
            "message": message,
            "data": data
        }
    )
