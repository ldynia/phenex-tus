from datetime import datetime
from mongoengine import Document
from mongoengine import DictField
from mongoengine import ListField
from mongoengine import EmailField
from mongoengine import StringField
from mongoengine import BooleanField
from mongoengine import DateTimeField
from mongoengine import ObjectIdField
from mongoengine import EmbeddedDocumentField
from mongoengine import DynamicEmbeddedDocument


class Metadata(DynamicEmbeddedDocument):
    meta = {
        'strict': False,
        'allow_inheritance': True
    }


class Service(Metadata):
    name = StringField()
    version = StringField()
    options = DictField()
    metadata = EmbeddedDocumentField(Metadata, default=Metadata())

    meta = {
        'strict': False,
        'allow_inheritance': False
    }


class Samples(Document):
    service = EmbeddedDocumentField(Service, default=Service())
    results_email = EmailField(required=False)
    # AUTHOR: ludd@food.dtu.dk
    # TODO: uploads field should be ListField(ReferenceField(UploadModel))
    uploads = ListField(ObjectIdField())
    fingerprints = ListField(StringField(), required=True)
    anonymous_id = StringField()
    # https://bitbucket.org/genomicepidemiology/phenex-api/src/master/app/components/frontend_user/frontend_user_model.py
    #uploaded_by = RefereneFiled(User)
    metadata = EmbeddedDocumentField(Metadata, default=Metadata())
    create_at = DateTimeField(default=datetime.utcnow())
    modified_at = DateTimeField(default=datetime.utcnow())
    upload_completed = BooleanField(default=False)
