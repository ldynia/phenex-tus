import base64
from flask import request
from flask_tus.models import MongoengineModel
from mongoengine import ListField
from mongoengine import StringField
from mongoengine import ReferenceField
from .sample_models import Samples


class UploadModel(MongoengineModel):
    samples = ListField(ReferenceField(Samples))
    anonymous_id = StringField()

    def add_reference_to_sample(self):
        sample_id = request.headers.get('Sample')
        sample_id = base64.b64decode(sample_id).decode('ascii')

        sample = Samples.objects.get(id=sample_id)
        if self.id not in sample.uploads:
            sample.uploads.append(self.id)
            sample.service.metadata['files'].append(self.path)

        # AUTHOR: ludd@food.dtu.dk
        # TODO: This is very naive way of detecting if sample upload has been finished.
        # The correct way would be to check if last fingerprint in samples equals to current uploading fingerprint.
        if len(sample.uploads) == len(sample.fingerprints):
            sample.upload_completed = True

        if sample not in self.samples:
            self.samples.append(sample)

        self.save()
        sample.save()
